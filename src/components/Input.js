import React, {Component} from 'react';
import { View, TextInput, Text, TouchableWithoutFeedback, Keyboard} from 'react-native';

import { Colors } from '../common/Colors';

const Input = ({label, value, onChangeText, placeholder, secure}) => {

    const {inputStyle, labelStyle, containerStyle} = styles;

    let labelBlock = null;

    if (label) {
        labelBlock = (
            <Text style={labelStyle}>
                {label}
            </Text>
        )
    }

    return (
        <TouchableWithoutFeedback XXonPress={Keyboard.dismiss}>
            <View style={containerStyle}>
                {labelBlock}
                <TextInput
                    underlineColorAndroid={'transparent'}
                    secureTextEntry={secure}
                    placeholder={placeholder}
                    style={inputStyle}
                    autoCorrect={false}
                    value={value}
                    onChangeText={onChangeText}
                />
            </View>
        </TouchableWithoutFeedback>
    )
};

const styles = {
    containerStyle: {
        padding: 0,
        marginVertical: 10,
    },
    labelStyle: {
        paddingHorizontal: 10,
        paddingVertical: 4,
        fontSize: 16,
        margin: 0,
        color: Colors[Colors.currentTheme].panelText,
        fontFamily: 'sans-serif-light',
    },
    inputStyle: {
        borderWidth: 0,
        backgroundColor: 'transparent',
        color: Colors[Colors.currentTheme].panelText,
        fontSize: 14,
        marginHorizontal: 10,
        marginVertical: 0,
        paddingHorizontal: 10,
        paddingVertical: 4,
        fontFamily: 'sans-serif-light',
    }
};


export { Input };