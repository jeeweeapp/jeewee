// import libraries
import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { Colors } from '../common/Colors';
import EStyleSheet from 'react-native-extended-stylesheet';

EStyleSheet.build();

// make a component
const SmallButton = ({onPress, active, children, onWhite, style}) => {
    const {buttonStyle, textStyle, activeButtonStyle} = styles;

    let touchStyle = buttonStyle;

    if (active) {
        touchStyle = activeButtonStyle;
    }

    if (onWhite) {
        touchStyle = [touchStyle, { borderColor: Colors[Colors.currentTheme].button.onWhite.link.border}]
    }

    return (
        <TouchableOpacity onPress={onPress} style={[touchStyle, style]}>
            <Text style={[
                    style,
                    textStyle,
                    active ?
                        onWhite ?
                        {
                            color: Colors[Colors.currentTheme].button.onWhite.link.textActive
                        } :
                        {
                            color: Colors[Colors.currentTheme].button.onDark.link.textActive
                        } : onWhite ?
                            {
                                color: Colors[Colors.currentTheme].button.onWhite.link.text
                            } : ''
                ]}>
                {children}
            </Text>
        </TouchableOpacity>
    );
};

const styles = EStyleSheet.create({
    buttonStyle: {
        alignSelf: 'flex-start',
        borderWidth: 0,
        paddingVertical: 5,
        paddingHorizontal: 2,
        elevation: 0,
        marginLeft: 10,
        marginVertical: 2,
        marginHorizontal: 5
    },
    textStyle: {
        textAlign: 'center',
        color: Colors[Colors.currentTheme].button.onDark.link.text,
        fontSize: 16,
        fontFamily: 'sans-serif-light',
    },
    activeButtonStyle: {
        alignSelf: 'flex-start',
        paddingVertical: 5,
        paddingHorizontal: 2,
        elevation: 0,
        marginLeft: 10,
        marginVertical: 2,
        marginHorizontal: 5,
        borderWidth: 0,
        borderBottomWidth: 1,
        borderColor: Colors[Colors.currentTheme].button.onDark.link.border,
    }
});

// make a component available to other parts of the app
export { SmallButton };