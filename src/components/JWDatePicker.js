import React, { Component } from 'react';
import DatePicker from 'react-native-datepicker';
import moment from 'moment';
import {Colors} from '../common/Colors';

const dateFormat = 'ddd, DD MMM';
const minDate = moment().format(dateFormat);

export default class JWDatePicker extends Component {
    render(){
        return (
            <DatePicker
                showIcon={false}
                style={this.props.style}
                date={this.props.date}
                mode="date"
                placeholder="select date"
                format={dateFormat}
                minDate={minDate}
                confirmBtnText="Confirm"
                cancelBtnText="Cancel"
                customStyles={{
                    dateInput: {
                        marginLeft: 10,
                        borderWidth: 0,
                        alignItems: 'flex-start'
                    },
                    dateText: {
                        padding: 0,
                        margin: 0,
                        fontFamily: 'sans-serif-light',
                        color: Colors[Colors.currentTheme].input.onWhite.text,
                        fontSize: 18
                    }
                }}
                onDateChange={this.props.onDateChange}
            />
        )
    }
}