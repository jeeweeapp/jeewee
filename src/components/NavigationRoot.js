import React, { Component } from 'react';
import {
    BackAndroid,
    NavigationExperimental,
    Text,
    View,
    Animated,
    StyleSheet
} from 'react-native';

import Home from '../scenes/Home';
import SearchAddress from '../scenes/SearchAddress';
import SearchSport from '../scenes/SearchSport';
import Activities from '../scenes/Activities';
import CreateActivity from '../scenes/CreateActivity';
import MyActivities from '../scenes/MyActivities';
import SearchVenue from '../scenes/SearchVenue';
import About from '../scenes/About';
import Venues from '../scenes/Venues';

const {
    Transitioner: NavigationTransitioner,
    Card: NavigationCard,
    Header: NavigationHeader
} = NavigationExperimental;

export default class NavigationRoot extends Component {
    constructor(props) {
        super(props);

        this._renderScene = this._renderScene.bind(this);
    }

    _renderScene({scene}) {
        const { route } = scene;

        switch(route.key) {
            case 'Home':
                return <Home />;
            case 'SearchAddress':
                return <SearchAddress />;
            case 'SearchSport':
                return <SearchSport/>;
            case 'Activities':
                return <Activities />;
            case 'CreateActivity':
                return <CreateActivity />;
            case 'SearchVenue':
                return <SearchVenue />;
            case 'MyActivities':
                return <MyActivities />;
            case 'Venues':
                return <Venues />
            case 'About':
                return <About />
        }
    };

    render() {

        let { navigation, backAction } = this.props;

        BackAndroid.addEventListener('hardwareBackPress', function() {
            // this.onMainScreen and this.goBack are just examples, you need to use your own implementation here
            // Typically you would use the navigator here to go to the last state.
            if (navigation.index !== 0) {
                backAction();
                return true;
            }
            return true;
        });

        return (

            // Redux is handling the reduction of our state for us. We grab the navigationState
            // we have in our Redux store and pass it directly to the <NavigationTransitioner />.
            <NavigationTransitioner
                navigationState={navigation}
                style={styles.container}
                render={props => (
					// This mimics the same type of work done in a NavigationCardStack component
					<View style={styles.container}>
                        <NavigationCard
                            // <NavigationTransitioner>'s render method passes `navigationState` as a
                            // prop to here, so we expand it plus other props out in <NavigationCard>.
                            {...props}
                            // Transition animations are determined by the StyleInterpolators. Here we manually
                            // override the default horizontal style interpolator that gets applied inside of
                            // NavigationCard for a vertical direction animation if we are showing a modal.
                            // (Passing undefined causes the default interpolator to be used in NavigationCard.)
                            style={props.scene.route.key === 'Modal' ?
                                        NavigationCard.CardStackStyleInterpolator.forVertical(props) :
                                        styles.card
                            }
                            onNavigateBack={backAction}
                            // By default a user can swipe back to pop from the stack. Disable this for modals.
                            // Just like for style interpolators, returning undefined lets NavigationCard override it.
                            panHandlers={props.scene.route.key === 'Modal' ? null : undefined }
                            renderScene={this._renderScene}
                            key={props.scene.route.key}
                        />
					</View>
				)}
            />
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    card: {
        backgroundColor: 'white'
    }
});