// import libraries
import React, {Component} from 'react';
import { Text, View, StyleSheet, Image, Menu} from 'react-native';

import EStyleSheet from 'react-native-extended-stylesheet';
EStyleSheet.build();
import { Colors } from '../common/Colors';

import { SearchBar, Icon } from 'react-native-elements';
import { navigatePop } from "../actions/NavigationActions";
import { connect } from 'react-redux';

// make a component
class Header extends Component {

    renderLeftIcon() {
        const {
            menuIconStyle
        } = styles;

        if (!this.props.navigateBack) {
            return (
                <Icon
                    name='menu'
                    color='#fff'
                    iconStyle={menuIconStyle}
                    size={45}
                    onPress={this.props.openMenu}
                />
            )
        } else {
            return (
                <Icon
                    name='arrow-back'
                    color='#fff'
                    iconStyle={menuIconStyle}
                    size={40}
                    onPress={this.props.navigatePop}
                />
            )
        }
    }

    render() {
        const {
            viewStyle,
            searchBoxStyle,
            searchInputStyle,
            iconStyle,
            imageStyle,
            imageContainerStyle
        } = styles;

        const {
            onChangeText,
            searchQuery,
            placeholder,
            navigateBack,
        } = this.props;

        let header = null;

        if (onChangeText) {
            header = (
                <View style={viewStyle}>
                    <Icon
                        name='arrow-back'
                        color='#fff'
                        iconStyle={iconStyle}
                        onPress={navigateBack}
                    />
                    <SearchBar
                        containerStyle={searchBoxStyle}
                        inputStyle={searchInputStyle}
                        icon={{style: {
                            left: 12,
                            top: 18
                        }}}
                        value={searchQuery}
                        onChangeText={onChangeText}
                        autoFocus={true}
                        placeholder={placeholder} />
                </View>
            )
        } else {
            header = (
                <View style={imageContainerStyle}>
                    {this.renderLeftIcon()}
                    <Image
                        resizeMode={Image.resizeMode.contain}
                        source={require('../images/logo-cut.png')}
                        style={imageStyle} />
                </View>
            )
        }
        return header
    }
};

const styles = StyleSheet.create({
    viewStyle: {
        flex: 0,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: Colors[Colors.currentTheme].header.bg,
        height: 60,
        elevation: 2,
        position: 'relative',
        padding: 0
    },
    searchBoxStyle: {
        flex: 1,
        borderBottomWidth: 0,
        borderTopWidth: 0,
        elevation: 0,
        backgroundColor: 'transparent',
    },
    searchInputStyle: {
        height: 40,
        backgroundColor: '#fff',
        fontFamily: 'sans-serif-light'
    },
    iconStyle: {
        flex: 0,
        margin: 0,
        width: 35,
        padding: 8,
        height: 40,
    },
    imageContainerStyle: {
        flex: 0,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: Colors[Colors.currentTheme].header.bg,
        height: 60,
    },
    imageStyle: {
        height: 40,
        width: 130,
    },
    menuIconStyle: {
        flex: 0,
        margin: 0,
        marginRight: 2,
        width: 60,
        paddingLeft: 10,
        paddingRight: 5,
        paddingTop: 7,
        paddingBottom: 5,
        color: Colors[Colors.currentTheme].header.text
    }
});

// wire Home to Store and wire the actions
export default connect(
    null,
    {
        navigatePop,
    }
)(Header);