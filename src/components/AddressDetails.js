import React from 'react';
import { View, Text } from 'react-native';

const AddressDetails = ({address}) => {

    // destructure the props
    const {rue, number, zip, city} = address;
    const { textStyle, viewStyle } = styles;

    return (
        <View style={viewStyle}>
            <Text style={textStyle}>{rue}, {number} - {zip} {city} </Text>
        </View>
    )
};

const styles = {
    viewStyle: {
        justifyContent: 'center',
        alignItems: 'center',
        height: 30,
        shadowColor: '#000000',
        shadowOffset: {
            width: 0,
            height: 5
        },
        shadowOpacity: 0.2,
        elevation: 1,
        position: 'relative'
    },
    textStyle: {
        fontSize: 12,
        fontFamily: 'sans-serif-light'
    }
};

export default AddressDetails;