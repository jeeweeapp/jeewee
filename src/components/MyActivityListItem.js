import React, { PropTypes } from 'react'
import { View, StyleSheet, Linking, TouchableHighlight, Image, Platform } from 'react-native'
import { Icon, Text, normalize} from 'react-native-elements'
import moment from 'moment';
import { Colors } from '../common/Colors';
import { Button } from '../components/Button';
import { SmallButton } from '../components/SmallButton';
import { toTitleCase, isInThePast} from '../utils/utils';

const timeFormat = 'HH:mm';

// custom config to show only the day without time
moment.locale('en', {
    calendar: {
        sameDay: function () {
            return '[Today]';
        },
        nextDay: function() {
            return '[Tomorrow]'
        },
        nextWeek: function() {
            // only the day of the week
            return 'dddd'
        },
        sameElse: function() {
            // only the day/month
            return 'D MMM'
        }
    }
});

const MyActivityListItem = ({
    activity,
    listIndex,
    onLeave,
    myOwnActivity
}) => {
    let Component = TouchableHighlight;
    const {
        sport,
        date_start,
        date_end,
        attendees
    } = activity;

    let renderLeaveButton = () => {
        return (
            <SmallButton
                style={{
                    paddingTop: 0,
                    marginVertical: 0,
                    paddingVertical: 0
                }}
                onWhite={true}
                onPress={
                    onLeave
                }
            >
                Leave
            </SmallButton>
        )
    };

    let attendeesIconColor = Colors[Colors.currentTheme].icons.gray;

    if (attendees.length > 1) {
        attendeesIconColor = Colors[Colors.currentTheme].icons.color;
    }

    let renderAttendees = () => {
        return (
            <View
                style={{
                    flexBasis: 110,
                    flexGrow: 0,
                    flexDirection: 'row',
                    alignItems: 'center'
                }}
            >
                <Text
                    style={{
                        fontFamily: 'sans-serif-light',
                        flex: 1
                    }}
                >
                { attendees.length }
                </Text>
                <Icon
                    containerStyle={{
                        flex: 0,
                        padding: 0,
                        margin: 0
                    }}
                    iconStyle={{
                        padding: 0,
                        marginLeft: 0,
                        marginRight: 7,
                        marginBottom: 0,
                    }}
                    size={20}
                    name={'people'}
                    color={attendeesIconColor}
                />
            </View>
        )
    };

    let hostIcon = null;

    if (myOwnActivity) {
        hostIcon = <Icon
            name={'home'}
            size={14}
            color={Colors[Colors.currentTheme].icons.darkGray}
            containerStyle={{
                marginLeft: 5
            }}

        />
    }

    return (
        <Component
            underlayColor={'white'}
            style={[
                styles.container,
                listIndex === 0 ? {
                    paddingTop: 0
                } : '',
                isInThePast(activity.date_end) ? {
                    opacity: 0.3 // dim events in the past
                } : ''
            ]}>
            <View
                style={{
                    flex: 1
                }}
            >
                <View style={styles.wrapper}>
                    { renderAttendees() }
                    <View style={styles.titleContainer}>
                        <View
                            style={{
                                flexDirection: 'row'
                            }}
                        >
                            <Text
                                style={styles.title}
                            >
                                {toTitleCase(sport)}
                            </Text>
                            { hostIcon }
                        </View>
                        <View
                            style={{
                                marginTop: 2,
                                flexDirection: 'row',
                                alignItems: 'flex-end'
                            }}
                        >
                            <Text
                                style={styles.subtitle}>
                                {
                                    moment(date_start).calendar()
                                }
                            </Text>
                            <Icon
                                iconStyle={{
                                    marginLeft: 10,
                                    marginRight: 5,
                                    marginBottom: 2,
                                }}
                                size={12}
                                name={'schedule'}
                                color={Colors[Colors.currentTheme].icons.color}
                            />
                            <Text
                                style={{
                                    fontFamily: 'sans-serif-light',
                                    marginRight: 8
                                }}
                            >
                                {
                                    moment(date_start).format(timeFormat)
                                }
                                <Text
                                    style={{
                                        color: '#ccc'
                                    }}
                                >
                                    {' - '}
                                </Text>
                                {
                                    moment(date_end).format(timeFormat)
                                }

                            </Text>
                        </View>
                    </View>
                    <View
                        style={{
                            flex: 0,
                            marginBottom: 20,
                            marginTop: 0,
                            padding: 0,
                        }}
                    >
                        { renderLeaveButton() }
                    </View>
                </View>
            </View>
        </Component>
    )
}

MyActivityListItem.defaultProps = {
    underlayColor: 'white'
};

MyActivityListItem.propTypes = {
    title: PropTypes.string,
    icon: PropTypes.any,
    onPress: PropTypes.func,
    subtitle: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
};

const styles = StyleSheet.create({
    container: {
        marginTop: 0,
        marginLeft: 0,
        paddingHorizontal: 25,
        paddingVertical: 10,
        borderBottomColor: '#ededed',
        borderBottomWidth: 1,
        backgroundColor: 'transparent'
    },
    wrapper: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    icon: {
        marginRight: 8
    },
    title: {
        fontSize: normalize(16),
        color: '#222',
        fontFamily: 'sans-serif-light'
    },
    subtitle: {
        fontSize: normalize(14),
        marginTop: 1,
        fontFamily: 'sans-serif-light',
        width: 70
    },
    titleContainer: {
        justifyContent: 'center',
        flex: 1,
    },
    rightContentContainer: {
        flex: 0,
        width: 95,
        flexDirection: 'column',
        alignItems: 'flex-start',
        justifyContent: 'flex-end',
        margin: 0
    },
    rightTitleContainer: {
        flex: 1,
        alignItems: 'flex-end',
        justifyContent: 'center'
    },
    rightTitleStyle: {
        marginRight: 5,
    }
})

export default MyActivityListItem
