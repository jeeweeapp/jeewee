// import libraries
import React, {Component} from 'react';
import { Text, View, TouchableHighlight} from 'react-native';
import { Colors } from '../common/Colors';
import {navigatePush, navigateJumpToKey} from "../actions/NavigationActions";
import { connect } from "react-redux";

// make a component
class JWMenu extends Component {


    render() {
        return (
            <View style={styles.jwmenu}>
                <Text style={styles.jwmenutitle}>
                    MENU
                </Text>
                <View
                    style={styles.jwmenucontent}
                >
                    <TouchableHighlight
                        style={styles.jwmenucontentbutton}
                        onPress={() => {
                            let r = this.props.navigation.routes;
                            if (r[r.length - 1 ].key === 'Home') {
                                this.props.closeMenu();
                            } else {
                                this.props.navigateHome();
                            }
                        }}
                        underlayColor={Colors[Colors.currentTheme].button.primary.bg}
                    >
                        <Text style={styles.jwmenucontenttext}>Home</Text>
                    </TouchableHighlight>
                    <TouchableHighlight
                        underlayColor={Colors[Colors.currentTheme].button.primary.bg}
                        style={styles.jwmenucontentbutton}
                        onPress={this.props.navigateMyActivities}
                    >
                        <Text style={styles.jwmenucontenttext}>My activities</Text>
                    </TouchableHighlight>
                    <TouchableHighlight
                        underlayColor={Colors[Colors.currentTheme].button.primary.bg}
                        style={styles.jwmenucontentbutton}
                        onPress={this.props.navigateAbout}
                    >
                        <Text style={styles.jwmenucontenttext}>About Jeewee</Text>
                    </TouchableHighlight>
                </View>
            </View>
        )
    }
};

const styles = {
    jwmenu: {
        flex: 1,
        backgroundColor: Colors[Colors.currentTheme].menu.bg,
        padding: 0,
        borderRightWidth: 1,
        borderRightColor: Colors[Colors.currentTheme].menu.border,
    },
    jwmenutitle: {
        color: Colors[Colors.currentTheme].menu.text,
        fontFamily: 'sans-serif-light',
        fontSize: 22,
        padding: 20,
        paddingTop: 15
    },
    jwmenucontent: {
        marginTop: 40
    },
    jwmenucontentbutton: {
        margin: 0,
        padding: 20,
        borderBottomWidth: 1,
        borderBottomColor: Colors[Colors.currentTheme].menu.border
    },
    jwmenucontenttext: {
        fontFamily: 'sans-serif-light',
        fontSize: 18,
        color: Colors[Colors.currentTheme].menu.links
    },
};

const mapStateToProps = (state) => {
    const { navigation } = state;
    return {
        navigation
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        navigateMyActivities: () => {
            dispatch(navigatePush('MyActivities'));
        },
        navigateAbout: () => {
            dispatch(navigatePush('About'));
        },
        navigateHome: () => {
            // TODO: if we are already at home close the menu
            if (true) {

            }
            dispatch(navigateJumpToKey('Home'));
        }
    }
};


// wire Home to Store and wire the actions
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(JWMenu);