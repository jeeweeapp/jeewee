import React, { PropTypes } from 'react'
import { View, StyleSheet, Linking, TouchableHighlight, Image, Platform } from 'react-native'
import { Icon, Text, normalize} from 'react-native-elements'
import moment from 'moment';
import { Colors } from '../common/Colors';
import { Button } from '../components/Button';
import { openLink } from '../utils/utils';
import Communications from 'react-native-communications';

const timeFormat = 'HH:mm';
const googleMapsLink = 'http://www.google.com/maps/place/';

// custom config to show only the day without time
moment.locale('en', {
    calendar: {
        sameDay: function () {
            return '[Today]';
        },
        nextDay: function() {
            return '[Tomorrow]'
        },
        nextWeek: function() {
            // only the day of the week
            return 'dddd'
        },
        sameElse: function() {
            // only the day/month
            return 'D MMM'
        }
    }
});

const VenueListItem = ({
    venue,
    onSelect,
    onPress,
    isOpen
}) => {

    let Component = TouchableHighlight;

    const {
        name,
        address,
        type,
        coord,
        phone,
        website,
        opening,
        indoor,
        outdoor,
        fee,
        booking
    } = venue;

    let renderSelectButton = () => {
        return (
            <Button
                onPress={onSelect}
            >
                {'Select this venue'}
            </Button>
        )
    };

    let renderOpeningDay = (day, index) => {

        var res = [];

        if (day.length < 1) {
            res.push(
                <Text
                    style={{
                        fontFamily: 'sans-serif-light'
                    }}
                >
                    Closed
                </Text>
            )
        }

        day.forEach((hours, key) => {
            res.push(
                <Text
                    style={{
                        fontFamily: 'sans-serif-light'
                    }}
                    key={key}>{hours.start} - {hours.end}
                </Text>
            );
        });

        return (
            <View
                style={{
                    flexDirection: 'row'
                }}
            >
                <Text
                    style={{
                        fontFamily: 'sans-serif-light',
                        width: 50
                    }}
                >
                    { moment().day(index+1).format('ddd') }
                </Text>
                { res }
            </View>
        )
    }

    let renderOpenings = () => {

        if (opening.default.length < 1) {
            return (
                <View
                    style={{
                        marginTop: 2,
                        marginBottom: 10,
                        flexDirection: 'row'
                    }}
                >
                    <Icon
                        iconStyle={{
                            marginRight: 5
                        }}
                        name={'schedule'}
                        color={Colors[Colors.currentTheme].icons.gray}
                        size={20}
                    />
                    <Text
                        style={{
                            fontFamily: 'sans-serif-light',
                            color: '#222',
                        }}
                    >
                        Always open
                    </Text>
                </View>
            )
        }

        let week = [
            opening.default,
            opening.default,
            opening.default,
            opening.default,
            opening.default,
            opening.default,
            opening.default
        ];

        if (opening.except) {
            opening.except.forEach((except) => {
                week[except.day - 1] = [];
                except.hours.forEach((hoursInterval) => {
                    week[except.day - 1].push(hoursInterval);
                });
            });
        }

        return (
            <View
                style={{
                    flexDirection: 'column',
                    marginBottom: 10
                }}
            >
                <Text
                    style={{
                        fontFamily: 'sans-serif-light',
                        color: Colors[Colors.currentTheme].text.dark
                    }}
                >
                    Openings
                </Text>
                {
                    week.map((day, index) => (
                        renderOpeningDay(day, index)
                    ))
                }
            </View>
        )
    };

    let renderInfoLinks = () => {

        let phoneBlock = null;

        let websiteBlock = null;

        if (phone) {
            phoneBlock = (
                <TouchableHighlight
                    underlayColor={'white'}
                    onPress={() => Communications.phonecall(phone, true)}
                >
                    <View
                        style={{
                            flexDirection: 'row',
                            marginVertical: 5,
                            marginRight: 5
                        }}
                    >
                        <Icon
                            iconStyle={{
                                paddingBottom: 5,
                            }}
                            name={'phone'}
                            color={Colors[Colors.currentTheme].icons.color}
                            size={20}
                        />
                        <Text
                            style={{
                                fontFamily: 'sans-serif-light',
                                color: Colors[Colors.currentTheme].icons.color,
                                marginLeft: 5
                            }}
                                >
                            {phone}
                        </Text>
                    </View>
                </TouchableHighlight>
            )
        }

        if (website) {
            websiteBlock = (
                <TouchableHighlight
                    underlayColor={'white'}
                    onPress={() => {
                        openLink(website);
                    }}
                >
                    <View
                        style={{
                            flexDirection: 'row',
                            marginVertical: 2,
                            marginHorizontal: 5
                        }}
                    >
                        <Icon
                            iconStyle={{
                                paddingBottom: 10,
                            }}
                            type={"material-community"}
                            name={'web'}
                            color={Colors[Colors.currentTheme].icons.color}
                            size={20}
                        />
                        <Text
                            style={{
                                fontFamily: 'sans-serif-light',
                                color: Colors[Colors.currentTheme].icons.color,
                                marginLeft: 5
                            }}
                        >
                            {website}
                        </Text>
                    </View>
                </TouchableHighlight>
            )
        }

        return (
            <View
                style={{
                    flexDirection: 'column',
                    justifyContent: 'flex-start',
                    alignItems: 'flex-start',
                }}
            >
                { phoneBlock }

                { websiteBlock }
            </View>
        )
    };

    let renderMap = () => {
        return (
            <TouchableHighlight
                underlayColor={'white'}
                onPress={() => {
                        openLink(googleMapsLink + coord.n + ',' + coord.e)
                    }
                }
            >
                <View
                    style={{
                        flex: 1,
                        flexDirection: 'row',
                        alignItems: 'center',
                        marginTop: 10,
                        marginLeft: 7
                    }}
                >
                    <Icon
                        iconStyle={{
                            flex: 1
                        }}
                        name={'place'}
                        color={Colors[Colors.currentTheme].icons.color}
                        size={20}
                    />
                    <Text
                        style={{
                            marginLeft: 2,
                            paddingRight: 5,
                            paddingBottom: 2,
                            fontFamily: 'sans-serif-light',
                            fontSize: 14,
                            color: Colors[Colors.currentTheme].icons.color
                        }}
                    >
                        Map
                    </Text>
                </View>
            </TouchableHighlight>
        )
    }

    let renderInOutDoor = () => {

        let result = '';
        let icon = 'home';
        let type = 'material';

        if (indoor && !outdoor) {
            result = 'Indoor only';
            icon = 'home';
        } else if(!indoor && outdoor) {
            result = 'Outdoor only';
            type = 'material-community';
            icon = 'pine-tree';
        } else {
            return null;
        }

        return (
            <View
                style={{
                        marginTop: 2,
                        marginBottom: 10,
                        flexDirection: 'row'
                    }}
            >
                <Icon
                    iconStyle={{
                        marginRight: 5
                    }}
                    name={icon}
                    type={type}
                    color={Colors[Colors.currentTheme].icons.gray}
                    size={20}
                />
                <Text
                    style={{
                        fontFamily: 'sans-serif-light',
                        marginTop: 2,
                        marginBottom: 10,
                        color: '#222'
                    }}
                >
                { result }
                </Text>
            </View>
        )
    };

    let renderBooking = () => {
        if (booking) {
            return (
                <View
                    style={{
                        marginTop: 2,
                        marginBottom: 10,
                        flexDirection: 'row'
                    }}
                >
                    <Icon
                        iconStyle={{
                            marginRight: 5
                        }}
                        name={'book'}
                        color={Colors[Colors.currentTheme].icons.gray}
                        size={20}
                    />
                    <Text
                        style={{
                            fontFamily: 'sans-serif-light',
                            marginTop: 2,
                            marginBottom: 10,
                            color: '#222'
                        }}
                    >
                        Booking required
                    </Text>
                </View>
            )
        } else {
            return null;
        }
    };

    let renderFee = () => {
        if (fee) {
            return (
                <View
                    style={{
                        marginTop: 2,
                        marginBottom: 10,
                        flexDirection: 'row'
                    }}
                >
                    <Icon
                        iconStyle={{
                            marginRight: 5
                        }}
                        name={'payment'}
                        color={Colors[Colors.currentTheme].icons.gray}
                        size={20}
                    />
                    <Text
                        style={{
                            fontFamily: 'sans-serif-light',
                            color: '#222'
                        }}
                    >
                        Fees might apply
                    </Text>
                </View>
            )
        } else {
            return null;
        }
    };

    let renderOpen = () => {
        if (isOpen) {
            return (
                <View
                    style={{
                        flex: 1,
                        flexDirection: 'column',
                        marginHorizontal: 0,
                        marginVertical: 5,
                        justifyContent: 'flex-start'
                    }}
                >

                    { renderOpenings() }

                    { renderInOutDoor() }

                    { renderBooking() }

                    { renderFee() }

                    { renderMap() }

                    { renderInfoLinks() }

                    <View
                        style={{
                            marginTop: 10
                        }}
                    >
                        {renderSelectButton()}
                    </View>
                </View>
            )
        } else {
            return null;
        }
    };

    return (
        <Component
            underlayColor={'white'}
            style={{
                margin: 0,
                padding: 0
            }}
            onPress={onPress}
        >
            {/* We need a wrapper view to avoid
            https://facebook.github.io/react-native/docs/direct-manipulation.html#composite-components-and-setnativeprops*/}
            <View
                style={{
                    paddingHorizontal: 20,
                    paddingVertical: 10,
                    borderTopWidth: 1,
                    borderTopColor: '#eee'
                }}
            >
                <View
                    style={{
                        flexDirection: 'row'
                    }}
                >
                    <Text
                        style={{
                            fontSize: 18,
                            fontFamily: 'sans-serif-light',
                            color: Colors[Colors.currentTheme].button.primary.bg,
                            flex: 1
                        }}
                    >
                        {name}
                    </Text>
                    <Text
                        style={{
                            flex: 1,
                            fontFamily: 'sans-serif-light',
                            fontSize: 12,
                            textAlign: 'right'
                        }}
                    >
                        {type}
                    </Text>
                </View>
                <Text
                    style={{
                        fontFamily: 'sans-serif-light',
                        color: '#999'
                    }}
                >
                    {address.street}{ address.number ? ' ' + address.number : ''}{ address.city ? ' - ' + address.city : ''}
                </Text>
                {renderOpen()}
            </View>
        </Component>
    )
};

VenueListItem.defaultProps = {
    underlayColor: 'white'
};

VenueListItem.propTypes = {
    title: PropTypes.string,
    icon: PropTypes.any,
    onPress: PropTypes.func,
    subtitle: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
};

const styles = StyleSheet.create({
    container: {
        marginLeft: 0,
        paddingTop: 10,
        paddingRight: 0,
        paddingBottom: 10,
        borderBottomColor: '#ededed',
        borderBottomWidth: 1,
        backgroundColor: 'transparent'
    },
    containerOpen: {
        marginLeft: 0,
        paddingTop: 10,
        paddingRight: 0,
        paddingBottom: 10,
        borderBottomColor: '#ededed',
        borderBottomWidth: 1,
        backgroundColor: 'transparent',
        minHeight: 150
    },
    wrapper: {
        flexDirection: 'row'
    },
    icon: {
        marginRight: 8
    },
    titleContainer: {
        justifyContent: 'center',
        flex: 1,
    },
    rightContentContainer: {
        flex: 0,
        width: 80,
        flexDirection: 'column',
        alignItems: 'flex-start',
        justifyContent: 'flex-end',
        margin: 0
    },
    rightTitleContainer: {
        flex: 1,
        alignItems: 'flex-end',
        justifyContent: 'center'
    },
    rightTitleStyle: {
        marginRight: 5,
    }
});

export default VenueListItem
