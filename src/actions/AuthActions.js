let DeviceInfo = require('react-native-device-info');
import ApiUtils from '../utils/ApiUtils';

let DEVICE_UUID = DeviceInfo.getUniqueID();
import {tokenKey, userIdKey} from '../config/config';

import Config from 'react-native-config';

import {
    TOKEN_GET_SUCCESS,
    TOKEN_GET_START,
    TOKEN_GET_FAIL,
    USERID_GET_SUCCESS,
    USERID_GET_FAIL,
    USERID_GET_START,
    LOADING_START,
    LOADING_STOP,
    SHOW_MESSAGE
} from './types';

import { getTimeoutPromise, networkErrorMessage} from '../utils/utils';

export const tokenGet = () => {
    return (dispatch) => {
        // We don't have a local token.
        // Get the token from the server.
        dispatch({ type: TOKEN_GET_START});
        dispatch({ type: USERID_GET_START});
        dispatch({ type: LOADING_START});

        // Perform the actual API call
        let requestToken = fetch(Config.API_URL + "/tokens", {
            method: "POST",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({"device_id": DEVICE_UUID})
        });

        Promise
            .race([getTimeoutPromise(), requestToken])
            .then(ApiUtils.checkStatus)
            .then((response) => response.json())
            .then((responseJson) => {
                let token = responseJson.data[tokenKey];
                let userID = responseJson.data[userIdKey];
                dispatch({ type: TOKEN_GET_SUCCESS, payload: token });
                dispatch({ type: USERID_GET_SUCCESS, payload: userID});
                dispatch({ type: LOADING_STOP});
            })
            .catch((error) => {
                if (!error.response) {
                    dispatch({ type: TOKEN_GET_FAIL, payload: error });
                    dispatch({ type: USERID_GET_FAIL, payload: error });
                    dispatch({ type: SHOW_MESSAGE, payload: networkErrorMessage });
                } else if (error.response.status === 400) {
                    dispatch(registerUser());
                }
            });
    };
};

const registerUser = () => {
    return (dispatch) => {
        // this device is not registered so we proceed with the registration API
        let requestRegistration = fetch(Config.API_URL + "/users", {
            method: "POST",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({"device_id": DEVICE_UUID})
        });

        Promise
            .race([getTimeoutPromise(), requestRegistration])
            .then(ApiUtils.checkStatus)
            .then((response) => response.json())
            .then((responseJson) => {
                let token = responseJson.data.initialToken;
                let userID = responseJson.data._id;
                dispatch({ type: TOKEN_GET_SUCCESS, payload: token });
                dispatch({ type: USERID_GET_SUCCESS, payload: userID });
                dispatch({ type: LOADING_STOP});
            })
            .catch((error) => {
                if (!error.response) {
                    console.log('RequestRegistration error: ' + JSON.stringify(error));
                } else {
                    console.log('RequestRegistration error: ' + JSON.stringify(error.response));
                }
            });
    }
};