import {
    ADDRESS_CHANGED,
    SPORT_CHANGED,
} from './types';

export const addressChanged = (address) => {
    return {
        type: ADDRESS_CHANGED,
        payload: address
    };
};

export const sportChanged = (sport) => {
    return {
        type: SPORT_CHANGED,
        payload: sport
    }
};