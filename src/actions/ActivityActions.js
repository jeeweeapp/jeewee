import moment from 'moment';
import { warn } from '../utils/utils';

import {
    ACTIVITIES_GET_START,
    ACTIVITIES_GET_SUCCESS,
    ACTIVITIES_GET_FAIL,

    ACTIVITY_JOIN_START,
    ACTIVITY_JOIN_SUCCESS,
    ACTIVITY_JOIN_FAIL,

    ACTIVITY_LEAVE_FAIL,
    ACTIVITY_LEAVE_START,
    ACTIVITY_LEAVE_SUCCESS,

    ACTIVITY_CREATE_START,
    ACTIVITY_CREATE_FAIL,
    ACTIVITY_CREATE_SUCCESS,
    ACTIVITY_CREATE_UPDATE_FORM,
    ACTIVITY_CREATE_CLEAN_FORM,

    MY_ACTIVITIES_GET_FAIL,
    MY_ACTIVITIES_GET_START,
    MY_ACTIVITIES_GET_SUCCESS,

    CLEAR_NOTIFICATION,

    GPS_SUCCESS,

    LOADING_STOP,
    LOADING_START,
    SHOW_MESSAGE

} from './types';

import { getTimeoutPromise, gpsFailMessage } from '../utils/utils';
import ApiUtils from '../utils/ApiUtils';
import { navigatePush } from '../actions'

import Config from 'react-native-config';

let DeviceInfo = require('react-native-device-info');
let DEVICE_UUID = DeviceInfo.getUniqueID();

import { getLinearDistanceBetween } from '../utils/utils';

export const activitiesGet = (origin, sport, token, fromDate) => {
    return (dispatch) => {
        dispatch({ type: ACTIVITIES_GET_START});

        let url = Config.API_URL + "/activities";
        let queryStrings = [];

        if (fromDate) {
            // date always in UTC since that is the timezone mongo uses
            let q = 'fromDate='+ moment(fromDate).toISOString();
            q = q.replace('+', '%2B');
            queryStrings.push(q);
        }

        if (sport && sport !== 'all') {
            let q = 'sport=' + sport;
            queryStrings.push(q);
        }

        if (queryStrings.length > 0) {
            url += '?';
            queryStrings.forEach(function(item, index){
                url += item;
                if (index !== queryStrings.length - 1) {
                    url += '&'
                }
            });
        }

        let request = fetch(
            url,
            {
                method: "GET",
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + token
                }
            }
        );

        return Promise
            .race([getTimeoutPromise(), request])
            .then(ApiUtils.checkStatus)
            .then((response) => response.json())
            .then((responseJson) => {
                let activities = responseJson.data;
                if (origin) {
                    activities = calculateDistances(origin, activities);
                }
                dispatch({ type: ACTIVITIES_GET_SUCCESS, payload: activities});
            })
            .catch((error) => {
                warn(error);
                if (!error.response) {
                    dispatch({ type: ACTIVITIES_GET_FAIL, payload: error });
                } else {
                    dispatch({ type: ACTIVITIES_GET_FAIL, payload: 'Something went wrong fetching activities' });
                }
            });
    }
};

export const activityJoin = (activity, token) => {
    return (dispatch) => {
        dispatch({ type: ACTIVITY_JOIN_START});
        let url = Config.API_URL + "/activities/" + activity._id + '/join';
        let request = fetch(
            url,
            {
                method: "PUT",
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + token
                },
                body: JSON.stringify({"device_id": DEVICE_UUID})
            }
        );

        // return is important if we want to to something
        // in the 'then' from where the action creator was called
        // In this case we want the refresh the activities (see Activities.js)
        return Promise
            .race([getTimeoutPromise(), request])
            .then(ApiUtils.checkStatus)
            .then((response) => response.json())
            .then((responseJson) => {
                dispatch({ type: ACTIVITY_JOIN_SUCCESS, payload: activity});
            })
            .catch((error) => {
                console.log(error);
                if (!error.response) {
                    dispatch({ type: ACTIVITY_JOIN_FAIL, payload: error });
                } else {
                    dispatch({ type: ACTIVITY_JOIN_FAIL, payload: 'Something went wrong joining the activity' });
                }
            });
    }
};

export const activityLeave = (activity, token) => {
    return (dispatch) => {
        dispatch({ type: ACTIVITY_LEAVE_START});
        let url = Config.API_URL + "/activities/" + activity._id + '/leave';
        let request = fetch(
            url,
            {
                method: "PUT",
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + token
                },
                body: JSON.stringify({"device_id": DEVICE_UUID})
            }
        );

        return Promise
            .race([getTimeoutPromise(), request])
            .then(ApiUtils.checkStatus)
            .then((response) => response.json())
            .then((responseJson) => {
                dispatch({ type: ACTIVITY_LEAVE_SUCCESS, payload: activity});
            })
            .catch((error) => {
                console.log(error);
                if (!error.response) {
                    dispatch({ type: ACTIVITY_LEAVE_FAIL, payload: error });
                } else {
                    dispatch({ type: ACTIVITY_LEAVE_FAIL, payload: 'Something went wrong leaving the activity' });
                }
            });
    }
};

export const myActivitiesGet = (userID, token) => {
    return (dispatch) => {
        dispatch({ type: MY_ACTIVITIES_GET_START});

        let now = moment().toISOString();
        let url = Config.API_URL + "/activities?user=" + userID + '&fromDate=' + now;

        let request = fetch(
            url,
            {
                method: "GET",
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + token
                }
            }
        );

        Promise
            .race([getTimeoutPromise(), request])
            .then(ApiUtils.checkStatus)
            .then((response) => response.json())
            .then((responseJson) => {
                let activities = responseJson.data;
                dispatch({ type: MY_ACTIVITIES_GET_SUCCESS, payload: activities});
            })
            .catch((error) => {
                console.log(error);
                if (!error.response) {
                    dispatch({ type: MY_ACTIVITIES_GET_FAIL, payload: error });
                } else {
                    dispatch({ type: MY_ACTIVITIES_GET_FAIL, payload: 'Something went wrong fetching my activities' });
                }
            });
    }
};

export const activityCreate = (data, token) => {
    let url = Config.API_URL + "/activities";
    return (dispatch) => {

        // wrap the GPS function into a promise.
        // If we have the GPS data or if the GPS works,
        // we resolve the Promise with the data and we proceed
        // sending the new activity form to the API.

        // If something goes bad we reject the GPS promise
        // and we stop the process with a message for the user.
        return new Promise(function(resolve, reject) {
            dispatch({type: ACTIVITY_CREATE_START});
            dispatch({ type: LOADING_START });

            if (data.place.coord === null) {
                // TODO: check if we can avoid callbacks and use promises or await
                navigator.geolocation.getCurrentPosition(
                    (position) => {
                        console.log("GPS returned " + JSON.stringify(position));
                        let androidCoords = position.coords;
                        data.place.coord = {
                            n: androidCoords.latitude,
                            e: androidCoords.longitude
                        };
                        dispatch({ type: GPS_SUCCESS, payload: data.place.coord});
                        return sendCreationFormToApi(url, data, token, dispatch);
                    },
                    (error) => {
                        warn(error);
                        reject(error);
                    },
                    {
                        enableHighAccuracy: false, // true was very slow
                        timeout: 5000,
                        maximumAge: 60 * 60 * 1000 // current location cached for 1 hour
                    }
                );
            } else {
                resolve();
            }
        }).then(() => {
            dispatch({type: CLEAR_NOTIFICATION});
            sendCreationFormToApi(url, data, token, dispatch);
        }).catch((error) => {
            console.log(error);
            dispatch({ type: SHOW_MESSAGE, payload: gpsFailMessage});
        }).then(() => {
            dispatch({ type: LOADING_STOP });
        });
    }
};

function sendCreationFormToApi(url, data, token, dispatch) {
    let request = fetch(
        url,
        {
            method: "POST",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token
            },
            body: JSON.stringify(data)
        }
    );

    return Promise
        .race([getTimeoutPromise(), request])
        .then(ApiUtils.checkStatus)
        .then((response) => response.json())
        .then((responseJson) => {
            dispatch({type: ACTIVITY_CREATE_SUCCESS, payload: 'New activity created!'});
            dispatch({type: ACTIVITY_CREATE_CLEAN_FORM});
            dispatch(navigatePush('Activities'));
        })
        .catch((error) => {
            warn(data);
            warn(error);
            if (!error.response) {
                dispatch({type: ACTIVITY_CREATE_FAIL, payload: error});
            } else {
                dispatch({type: ACTIVITY_CREATE_FAIL, payload: 'Something went wrong creating an activitiy'});
            }
        });
}

export const clearNotification = () => {
    return (dispatch) => {
        dispatch({type: CLEAR_NOTIFICATION});
    }
}

export const updateForm = (form) => {
    return {
        type: ACTIVITY_CREATE_UPDATE_FORM,
        payload: form
    };
}

function calculateDistances(origin, destinations){
    destinations.forEach(function(destination){
        if (destination.place === null) {
            return;
        }
        let destCoord = destination.place.coord;
        if (destCoord !== null) {
            destination['distance'] = getLinearDistanceBetween(origin, destCoord);
        } else {
            console.warn('This destination misses the coord: ' + JSON.stringify(destination));
        }
    });
    return destinations;
}