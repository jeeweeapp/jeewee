import {
    ADDRESS_VENUE_CHANGED,
    VENUE_SELECTED,
    VENUES_GET_START,
    VENUES_GET_SUCCESS,
    VENUES_GET_FAIL
} from './types';

import { getTimeoutPromise, warn } from '../utils/utils';
import ApiUtils from '../utils/ApiUtils';
import Config from 'react-native-config';

const VENUES_LIMIT = 10;

export const addressVenueChanged = (address) => {
    return {
        type: ADDRESS_VENUE_CHANGED,
        payload: address
    };
};

export const selectVenue = (venue) => {
    return (dispatch) => {
        dispatch({ type: VENUE_SELECTED, payload: venue });
    }
};

export const getVenues = (addressVenue, sport, token) => {
    return (dispatch) => {
        dispatch({ type: VENUES_GET_START});


        let url = Config.API_URL + "/venues";

        if (sport && addressVenue) {
            url = Config.API_URL + "/venues?sport=" + sport + "&e=" + addressVenue.coord.e + '&n=' + addressVenue.coord.n + '&limit=' + VENUES_LIMIT;
        }

        let request = fetch(
            url,
            {
                method: "GET",
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + token
                }
            }
        );

        Promise
            .race([getTimeoutPromise(), request])
            .then(ApiUtils.checkStatus)
            .then((response) => response.json())
            .then((responseJson) => {
                let venues = responseJson.data;
                dispatch({ type: VENUES_GET_SUCCESS, payload: venues});
            })
            .catch((error) => {
                warn(error);
                if (!error.response) {
                    dispatch({ type: VENUES_GET_FAIL, payload: error });
                } else {
                    dispatch({ type: VENUES_GET_FAIL, payload: 'Something went wrong fetching venues' });
                }
            });
    }
};

