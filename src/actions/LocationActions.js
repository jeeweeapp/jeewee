import {
    GPS_START,
    GPS_SUCCESS,
    GPS_ERROR,
    LOADING_START,
    LOADING_STOP,
    SHOW_MESSAGE
} from './types';

import {gpsFailMessage} from '../utils/utils';

export const getCurrentLocation = () => {
    return (dispatch) => {
        dispatch({ type: GPS_START});
        dispatch({ type: LOADING_START});
        console.log('Using GPS to get current coordinates...');

        return new Promise((resolve, reject) => {

            navigator.geolocation.getCurrentPosition(
                (position) => {
                    console.log("GPS returned " + JSON.stringify(position));
                    let androidCoords = position.coords;
                    let coord = {
                        n: androidCoords.latitude,
                        e: androidCoords.longitude
                    };
                    dispatch({type: GPS_SUCCESS, payload: coord});
                    dispatch({type: LOADING_STOP});
                    dispatch({type: SHOW_MESSAGE, payload: null});
                    resolve('GPS success');
                },
                (error) => {
                    dispatch({type: LOADING_STOP});
                    dispatch({type: SHOW_MESSAGE, payload: gpsFailMessage});
                    console.log(JSON.stringify(error));
                    dispatch({type: GPS_ERROR});
                    reject('GPS success');
                },
                {
                    enableHighAccuracy: false, // true was very slow
                    timeout: 5000,
                    maximumAge: 60 * 60 * 1000 // current location cached for 1 hour
                }
            );
        });
    };
};