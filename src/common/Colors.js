// taken from https://yeun.github.io/open-color/#yellow
import Color from 'color';


const cHappy = '#6FC178';
const cFluo = '#92C83D';
const cGrass = '#4D8C3F';

var ccGrass = Color(cGrass);
var ccFluo = Color(cFluo);
var ccWhite = Color('white');

export const Colors = {
    currentTheme: 'fluo',
    lightTheme: {
        home: {
            bg: '#d0df9a'
        }
    },
    fluo: {
        home: {
            bg: cFluo
        },
        text: {
            light: '#999',
            dark: '#222',
            color: cGrass
        },
        button: {
            onDark: {
                link: {
                    textActive: 'white',
                    text: ccGrass.lighten(1.2),
                    border: 'white'
                },
            },
            onWhite: {
                link: {
                    textActive: cHappy,
                    text: cGrass,
                    border: cGrass
                },
            },
            primary: {
                bg: cGrass,
                text: 'white'
            },
            disabled: {
                bg: cGrass,
                text: 'white'
            },
            leave: {
                bg: ccFluo.desaturate(0.8).lighten(0.3),
                text: 'white'
            }
        },
        input: {
            onWhite: {
                placeholder: ccWhite.darken(0.3),
                border: '#ddd',
                bg: 'transparent',
                text: cHappy,
            },
            onDark: {
                placeholder: ccGrass.darken(0.1),
                bg: 'transparent',
                border: '#d2e9c1',
                text: '#FFFFFF',
            }
        },
        warning: {
            bg: '#eeeedd',
            text: '#444'
        },
        notification: {
            bg: '#eeeedd',
            text: '#444',
            success: {
                bg: '#f0f0f0',
                text: '#557755'
            }
        },
        header: {
            bg: '#92C83D',
            text: '#FFFFFF'
        },
        icons: {
            color: cFluo,
            clickable: cGrass,
            gray: '#ccc',
            darkGray: '#666'
        },
        noResults: {
            text: '#aaa'
        },
        menu: {
            bg: 'white',
            border: ccGrass.lighten(1.2),
            text: ccGrass.darken(0.5),
            links: cGrass
        }
    }
};