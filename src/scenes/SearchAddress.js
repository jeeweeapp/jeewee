import React, { Component } from 'react';
import { View, ScrollView, Text, TouchableHighlight, StyleSheet } from 'react-native';
import Header from '../components/Header';

import { List, ListItem, Icon } from 'react-native-elements';

import { Colors } from '../common/Colors';

import addressUtils from '../utils/addressUtils';
import { navigatePop, addressChanged } from '../actions';

// limit the List rows to prevent app from crashing
// TODO: we might use some pagination mechanism
const MAX_ROWS = 200;

const DATA = require('../../data/lux-ville-shortid.min.json');
import {connect} from "react-redux";
import debounce from "lodash/debounce";

class SearchAddress extends Component {
    constructor(props, context) {
        super(props, context);

        // component state indicates data that is not meant to be shared
        // with other components
        // the search will lead to some result and the selected
        // address will be stored in the redux store
        this.state = {
            searchResults: null,
            searchQuery: ''
        };

        // debouce prevents to start searches if the user types faster than X milliseconds
        this.startSearch = debounce(this.startSearch, 200);
    }

    updateText(text) {
        this.setState({searchQuery: text}, () => {
            this.startSearch();
        });
    }

    startSearch() {
        let searchResults = addressUtils.searchAddress(DATA, this.state.searchQuery);
        this.setState({searchResults});
    }

    selectCurrentLocation() {
        this.props.addressChanged({street: null, coord: null});
        this.props.navigateBack();
    }

    renderUseCurrentAddress() {
        return (
            <TouchableHighlight
                underlayColor={'#eee'}
                onPress={this.selectCurrentLocation.bind(this)}
            >
                <View
                    style={styles.useCurrentAddress}
                >
                    <Icon
                        size={25}
                        name='my-location'
                        color={styles.useCurrentAddressIcon.color}
                        iconStyle={styles.useCurrentAddressIcon}
                        containerStyle={styles.useCurrentAddressIconContainer}
                    />
                    <Text
                        style={styles.useCurrentAddressText}
                    >
                        Use current location
                    </Text>
                </View>
            </TouchableHighlight>
        );
    }

    renderAddresses() {
        if (this.state.searchResults === null) {
            return (
                this.renderUseCurrentAddress()
            )
        } else if (this.state.searchResults.length > 0) {
            if (this.state.searchResults.length > MAX_ROWS) {
                // warning in case of too many results
                return (
                    <Text style={styles.noResult}>
                        There are too many addresses to show.{"\n"}
                        Please try adding the house number to narrow your search...
                    </Text>
                )
            } else {
                // normal result set
                return (
                    <List containerStyle={{marginBottom: 20}}>
                        {
                            this.state.searchResults.map((l, i) => (
                                <ListItem
                                    titleStyle={{
                                        fontFamily: 'sans-serif-light'
                                    }}
                                    onPress={() => {
                                        this.props.addressChanged(
                                                {
                                                    "street": l.rue + ' ' + l.number,
                                                    "coord": l.coord
                                                }
                                            );
                                        this.props.navigateBack();
                                        }
                                    }
                                    key={i}
                                    title={l.rue + ' ' + l.number + ' - ' + l.zip + ' ' + l.city}
                                />
                            ))
                        }
                    </List>
                )
            }
        } else if (this.state.searchQuery.length > 0) {
            // warning in case of 0 results
            return <Text style={styles.noResult}
                >No addresses matched your search.{"\n"}
                Try typing 1 word of at least 3 characters...</Text>
        } else {
            // the user did't type any character, no need to show the warning
            return true;
        }
    }

    backWithResults(results) {
        this.props.route.popCallback(results);
        this.props.navigateBack();
    }

    render() {
         return (
             <View>
                 <Header
                     searchQuery={this.state.searchQuery}
                     onChangeText={this.updateText.bind(this)} // TODO: not sure if calling fuctions like this is ok
                     navigateBack={this.props.navigateBack}
                     placeholder="Enter an address..."
                 />
                 <ScrollView
                     // with these properties clicking on a list item
                     // will directly trigger the onPress instead of dismissing
                     // first the keyboard
                     keyboardDismissMode={'on-drag'}
                     keyboardShouldPersistTaps={'always'}>

                     { this.renderAddresses() }

                 </ScrollView>
             </View>
         );
    }
}

SearchAddress.defaultProps = {
    address: {
        street: null,
        coord: null
    }
};

const styles = StyleSheet.create({
    noResult: {
        padding: 20,
        backgroundColor: Colors[Colors.currentTheme].warning.bg,
        color: Colors[Colors.currentTheme].warning.text
    },
    useCurrentAddress: {
        padding: 20,
        borderBottomWidth: 1,
        borderBottomColor: '#ccc',
        flex: 1,
        flexDirection: 'row'
    },
    useCurrentAddressText: {
        fontFamily: 'sans-serif-light',
        color: Colors[Colors.currentTheme].warning.text,
        flex: 1,
        fontSize: 18,
    },
    useCurrentAddressIconContainer: {
        flex: 0,
        flexBasis: 120,
        flexDirection: 'row'
    },
    useCurrentAddressIcon: {
        flex: 1,
        color: Colors[Colors.currentTheme].icons.color,
    }
});

const mapStateToProps = (state) => {
    const { address } = state;

    return {
        address
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        navigateBack: () => {
            dispatch(navigatePop());
        },
        addressChanged: (address) => {
            dispatch(addressChanged(address));
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(SearchAddress);