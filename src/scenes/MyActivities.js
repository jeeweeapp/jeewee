import React, { Component } from 'react';
import { View, ScrollView, Text } from 'react-native';
import { connect } from 'react-redux';
import Header from '../components/Header';
import { JWList } from '../components/JWList';
import MyActivityListItem from '../components/MyActivityListItem';
import { myActivitiesGet, activityLeave } from '../actions';
import { Colors } from '../common/Colors';

class MyActivities extends Component {

    constructor(props, context) {
        super(props, context);

        this.state = {
            myActivities: []
        };
    }

    componentWillMount() {
        this.getMyActivities();
    }

    getMyActivities() {
        let { userID, token} = this.props;
        this.props.myActivitiesGet(userID, token);
    }

    renderResults() {
        if (this.props.myActivities.length === 0) {
            return (
                <View>
                    <Text style={styles.noResultsMessage}>
                        {"Whoa! Looks like you haven't join an activity yet"}
                    </Text>
                </View>
            )
        } else {
            return (
                <ScrollView
                    style={{
                        borderWidth: 0,
                        paddingHorizontal: 0,
                        paddingVertical: 0,
                        margin: 0
                    }}
                >
                    <JWList>
                        {
                            this.props.myActivities.map((activity, index) => (
                                <MyActivityListItem
                                    key={activity._id}
                                    listIndex={index}
                                    activity={activity}
                                    onLeave={() => {
                                        var that = this;
                                        this.props.activityLeave(activity, this.props.token)
                                        .then(function(){
                                            // get the actvities again after joining
                                            // to refresh the view
                                            that.getMyActivities();
                                        })
                                    }}
                                    myOwnActivity={activity.host_id === this.props.userID}
                                />
                            ))
                        }
                    </JWList>
                </ScrollView>
            )
        }
    }

    renderMyActivities() {
        return (
            <View style={{backgroundColor: 'white', marginTop: 10}}>
                { this.renderResults()}
            </View>
        )
    }

    render() {
        return (
            <View
                style={{
                    flex: 1
                }}
            >
                <Header
                    navigateBack={true}
                />
                <View
                    style={{
                        padding: 0 // so the scrollbar stays on the border
                    }}
                    keyboardDismissMode={'on-drag'}
                    keyboardShouldPersistTaps={'always'}
                >
                    <Text
                        style={{
                            fontFamily: 'sans-serif-light',
                            color: '#222',
                            fontSize: 22,
                            paddingHorizontal: 25,
                            paddingTop: 20
                        }}
                    >
                        My future activities
                    </Text>

                    { this.renderMyActivities() }

                </View>
            </View>
        )
    }
}

const styles = {
    noResultsMessage: {
        padding: 20,
        paddingHorizontal: 25,
        fontFamily: 'sans-serif-light',
        fontSize: 18,
        fontStyle: 'italic',
        color: Colors[Colors.currentTheme].noResults.text
    },
};

const mapStateToProps = (state) => {
    const { token, userID } = state.auth;
    const { myActivities } = state.activities;
    return { token, userID, myActivities};
};


// wire Home to Store and wire the actions
export default connect(
    mapStateToProps,
    {
        myActivitiesGet,
        activityLeave
    }
)(MyActivities);