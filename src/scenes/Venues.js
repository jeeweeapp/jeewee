import React, { Component } from 'react';
import { View, ScrollView, Text, StyleSheet, TouchableHighlight } from 'react-native';
import Header from '../components/Header';
import { List, ListItem, Icon} from 'react-native-elements';
import { Colors } from '../common/Colors';
import {
    navigatePop,
    navigateJumpToKey,
    selectVenue,
    getVenues
} from '../actions';
import { JWList } from '../components/JWList';
import VenueListItem from '../components/VenueListItem';
// limit the List rows to prevent app from crashing
// TODO: we might use some pagination mechanism
const MAX_ROWS = 200;
import {connect} from "react-redux";

class Venues extends Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            venues: [],
            openVenue: null
        };
    }

    componentWillMount() {
        // fetch the closest 10 venues given the chosen address
        this.props.getVenues(this.props.addressVenue, this.props.form.sport, this.props.token);
    }

    renderTitle() {
        return (
            <TouchableHighlight
                onPress={ () => {
                        this.selectAddressOnlyVenue(this.props.addressVenue);
                    }
                }
                underlayColor={'#eee'}
                style={{
                    paddingLeft: 20,
                    padding: 10
                }}
            >
                <Text
                    style={{
                        fontFamily: 'sans-serif',
                        fontSize: 18,
                        color: Colors[Colors.currentTheme].button.primary.bg
                    }}
                >
                    USE THIS ADDRESS
                </Text>
            </TouchableHighlight>
        )
    }

    renderSummary() {
        const { city, street, number } = this.props.addressVenue.address;
        const { coord } = this.props.addressVenue;

        return (
            <View style={styles.summary}>
                <View>
                    <Text style={styles.summaryAddress}>
                        {street ? street + ' ' + number : coord ? 'East: ' + coord.e.toFixed(4) + ', North: ' + coord.n.toFixed(4) : ''}
                    </Text>
                    <Text
                        style={[styles.summaryAddress, { fontSize: 12 }]}
                    >
                        {city ? city : '' }
                    </Text>
                </View>
                <Icon
                    size={24}
                    name='edit'
                    color={styles.summaryIcon.color}
                    iconStyle={styles.summaryIcon}
                    onPress={this.props.navigateBack}
                />
            </View>
        )
    }

    renderVenuesTitle() {
        if (this.props.venues.length === 0) {
            return null
        } else {
            return (
                <Text
                    style={styles.listTitle}
                >
                    Or pick a venue nearby suitable for {this.props.form.sport}:
                </Text>
            )
        }
    }

    selectVenue(venue) {
        this.props.selectVenue(venue);
        this.props.navigateToCreation();
    }

    selectAddressOnlyVenue(addressVenue) {
        let venue = {
            address: addressVenue.address,
            coord: addressVenue.coord
        };

        this.selectVenue(venue);
    }

    renderVenuesList() {
        if (this.props.venues.length === 0) {
            return null;
        } else {
            return (
                // Keep in mind that ScrollViews must have a bounded height in order to work,
                // since they contain unbounded-height children into a bounded container (via a scroll interaction).
                // In order to bound the height of a ScrollView, either set the height of the
                // view directly (discouraged) or make sure all parent views have bounded height.
                // Forgetting to transfer {flex: 1} down the view stack can lead to errors here,
                // which the element inspector makes easy to debug.
                <ScrollView
                    keyboardShouldPersistTaps={'always'}
                    style={{
                        flex: 1,
                        borderWidth: 0,
                        paddingHorizontal: 0,
                        paddingVertical: 0,
                        marginBottom: 10
                    }}
                >
                    <JWList>
                        {
                            this.props.venues.map((venue, index) => (
                                <VenueListItem
                                    isOpen={this.state.openVenue === venue._id}
                                    key={venue._id}
                                    venue={venue}
                                    onSelect={() => {
                                        this.selectVenue(venue);
                                    }}
                                    onPress={() => {
                                        this.openVenueDetails(venue._id)
                                    }}
                                />
                            ))
                        }
                    </JWList>
                </ScrollView>
            )
        }
    }

    openVenueDetails(venueId) {
        this.setState({openVenue: venueId});
    }

    render() {
         return (
             <View
                 style={{
                    flex: 1,
                 }}
             >
                 <Header
                     navigateBack={this.props.navigateBack}
                 />
                 <View
                    style={{
                        flex: 1,
                    }}
                 >
                     { this.renderSummary() }

                     { this.renderTitle() }

                     { this.renderVenuesTitle() }

                     { this.renderVenuesList() }
                 </View>
             </View>
         );
    }
}

Venues.defaultProps = {
    addressVenue: {
        address: {
            street: null
        },
        coord: null
    }
};

const noResultStyle = {
    padding: 20,
    backgroundColor: Colors[Colors.currentTheme].warning.bg,
    color: Colors[Colors.currentTheme].warning.text
};

const styles = StyleSheet.create({
    summary: {
        paddingLeft: 20,
        paddingTop: 20,
        flexDirection: 'row',
        alignItems: 'center'
    },
    summaryIcon: {
        color: Colors[Colors.currentTheme].icons.gray
    },
    summaryAddress: {
        marginRight: 20,
        fontFamily: 'sans-serif-light',
        fontSize: 18
    },
    listTitle: {
        paddingTop: 30,
        marginBottom: 10,
        paddingHorizontal: 20,
        fontFamily: 'sans-serif-light',
    }
});

const mapStateToProps = (state) => {
    const { addressVenue, selectedVenue, venues } = state.venues;
    const { token } = state.auth;
    const { form } = state.activities;

    return {
        addressVenue,
        selectedVenue,
        venues,
        token,
        form
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        navigateBack: () => {
            dispatch(navigatePop());
        },
        selectVenue: (venue) => {
            dispatch(selectVenue(venue));
        },
        navigateToCreation: () => {
            dispatch(navigateJumpToKey('CreateActivity'));
        },
        getVenues: (addressVenue, sport, token) => {
            dispatch(getVenues(addressVenue, sport, token));
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Venues);