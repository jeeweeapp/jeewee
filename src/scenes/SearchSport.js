import React, { Component } from 'react';
import { View, ScrollView, Text } from 'react-native';
import Header from '../components/Header';
import { List, ListItem } from 'react-native-elements';
import { Colors } from '../common/Colors';
import {connect} from "react-redux";
import { filterByPropNameWithRegex, toTitleCase } from '../utils/utils';
import { navigatePop, navigateJumpToKey, sportChanged, navigateForgetLast } from '../actions';
import debounce from "lodash/debounce";

let sportsData = require('../../data/sports.json');

class SearchSport extends Component {

    constructor(props, context) {
        super(props, context);

        this.state = {
            sports: sportsData,
            searchQuery: '',
        };

        // debouce prevents to start searches if the user types faster than X milliseconds
        this.startSearch = debounce(this.startSearch, 200);
    }

    updateText(text) {
        this.setState({searchQuery: text}, () => {
            this.startSearch();
        });
    }

    startSearch() {
        const { searchQuery } = this.state;
        let searchResults = [];
        // if the results are empty because the search string was empty
        // we show all the available sports
        if (searchQuery.length < 1) {
            searchResults = sportsData;
        } else {
            this.setState({sports: sportsData, searchQuery: searchQuery});
            searchResults = filterByPropNameWithRegex(sportsData, searchQuery);
        }

        this.setState({ sports: searchResults});
    }

    renderSports() {
        if (this.state.sports.length > 0) {
            // normal result set
            return (
                <List containerStyle={{marginBottom: 20}}>
                    {
                        this.state.sports.map((l, i) => (
                            <ListItem
                                titleStyle={{
                                    fontFamily: 'sans-serif-light'
                                }}
                                onPress={() => {
                                    // this modifies the global sport (used when searching)
                                    this.props.sportChanged(l);

                                    // since the last screen should now stay on top of the stack
                                    // the back button should bring on creation or on home
                                    // properly
                                    this.props.navigateBack();
                                }}
                                key={i}
                                title={toTitleCase(l)}
                            />
                        ))
                    }
                </List>
            )
        } else if (this.state.searchQuery.length > 0) {
            // warning in case of 0 results
            return <Text style={noResultStyle}
            >No sports matched your search.{"\n"}
                Try typing 1 word of at least 3 characters...</Text>
        }
    }

    render() {
        return (
            <View>
                <Header
                    searchQuery={this.state.searchQuery}
                    onChangeText={this.updateText.bind(this)}
                    navigateBack={this.props.navigateHome}
                    placeholder="Type to search a sport"
                />
                <ScrollView
                    // with these properties clicking on a list item
                    // will directly trigger the onPress instead of dismissing
                    // first the keyboard
                    keyboardDismissMode={'on-drag'}
                    keyboardShouldPersistTaps={'always'}>

                    { this.renderSports() }

                </ScrollView>
            </View>
        );
    }
}

const mapStateToProps = () => {
    return {};
};

const mapDispatchToProps = (dispatch) => {
    return {
        navigateBack: () => {
            dispatch(navigatePop());
        },
        navigateHome: () => {
            dispatch(navigateJumpToKey('Home', true));
        },
        addressChanged: (address) => {
            dispatch(addressChanged(address));
        },
        sportChanged: (sport) => {
            dispatch(sportChanged(sport));
        },
    }
};

const noResultStyle = {
    padding: 20,
    backgroundColor: Colors[Colors.currentTheme].warningBg,
    color: Colors[Colors.currentTheme].warningText
};

export default connect(mapStateToProps, mapDispatchToProps)(SearchSport);