import {
    LOADING_START,
    LOADING_STOP,
    SHOW_MESSAGE,
    CLEAR_NOTIFICATION
} from '../actions/types';

const INITIAL_STATE = {
    loading: false,
    message: null
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case LOADING_START:
            return { ...state, loading: true };
        case LOADING_STOP:
            return { ...state, loading: false };
        case SHOW_MESSAGE:
            return { ...state, loading: false, message: action.payload };
        case CLEAR_NOTIFICATION:
            return { ...state, loading: false, message: null };
        default:
            return state;
    }
};