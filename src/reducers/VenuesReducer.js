import {
    ADDRESS_VENUE_CHANGED,
    VENUE_SELECTED,
    VENUES_GET_SUCCESS
} from '../actions/types';

const INITIAL_STATE = {
    addressVenue: {
        address: {
            street: null
        },
        coord: null
    },
    selectedVenue: null,
    venues: []
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case ADDRESS_VENUE_CHANGED:
            return { ...state, addressVenue: action.payload};
        case VENUE_SELECTED:
            return { ...state, selectedVenue: action.payload};
        case VENUES_GET_SUCCESS:
            return { ...state, venues: action.payload };
        default:
            return state;
    }
};
