import {
    OFFLINE,
} from '../actions/types';

const INITIAL_STATE = {
    online: true
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case OFFLINE:
            return { online: false };
        default:
            return state;
    }
};
