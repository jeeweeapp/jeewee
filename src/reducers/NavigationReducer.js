import {
    NAV_PUSH,
    NAV_POP,
    NAV_JUMP_TO_KEY,
    NAV_JUMP_TO_INDEX,
    NAV_RESET,
} from '../actions';

import * as NavigationStateUtils from 'NavigationStateUtils';

const initialState = {
    index: 0,
    key: 'root',
    onDone: null, // used in some case to pass a callback (e.g. after searching for a sport)
    routes: [
        {
            key: 'Home',
            title: 'Home',
        }
    ]
};

export default (state = initialState, action) => {
    switch (action.type) {
        case NAV_PUSH:
            if (state.routes[state.index].key === (action.state && action.state.key)) {
                return state;
            }

            if (NavigationStateUtils.has(state, action.state.key)) {
                // we already have the route in the list so we cannot push
                // but we jump there
                return jumpToKey(state, action);
            }

            return NavigationStateUtils.push(state, action.state);

        case NAV_POP:
            if (state.index === 0 || state.routes.length === 1) {
                return state;
            }
            return NavigationStateUtils.pop(state);

        case NAV_JUMP_TO_KEY:
            return jumpToKey(state, action);

        case NAV_JUMP_TO_INDEX:
            return NavigationStateUtils.jumpToIndex(state, action.index);

        default:
            return state
    }
}

function jumpToKey(state, action) {
    let newRoutes = state.routes;

    // bring the new route to the top of the stack
    newRoutes = bringKeyToTop(newRoutes, action.state.key);


    let newState = {
        ...state,
        routes: newRoutes
    };

    return NavigationStateUtils.jumpTo(newState, action.state.key);
}

// TODO: this is quite a piece of magic.
// Refactor to a proper navigation system
function bringKeyToTop(routes, key) {
    let matchingRoute = null;
    let newRoutes = routes;

    if (routes.length > 1) {
        routes.forEach((route, index) => {
            if (route.key === key) {
                // extract the matching route from the array
                matchingRoute = route;

                if (key === 'Home') {
                    // Special case for home: resets all the other routes
                    newRoutes = [ matchingRoute ];
                } else {
                    newRoutes.splice(index, 1);
                    // restore the route at the top
                    newRoutes.push(matchingRoute);
                }
            }
        });
    }
    return newRoutes;
}