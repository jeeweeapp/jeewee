import {PropTypes} from 'react';
import { connect } from 'react-redux';
import NavigationRoot from '../components/NavigationRoot';
import { navigatePush, navigatePop } from '../actions/NavigationActions';
function mapStateToProps(state) {
    return {
        navigation: state.navigation,
    };
}

NavigationRoot.propTypes = {
    navigation: PropTypes.object,
    backAction: PropTypes.func.isRequired
}

function mapDispatchToProps(dispatch) {
    return {
        backAction: () => dispatch(navigatePop()),
    };
}
export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(NavigationRoot);