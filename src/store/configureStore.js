import { createStore, applyMiddleware } from 'redux';
import rootReducer from '../reducers';
import thunk from 'redux-thunk';

export default function configureStore() {
    // const createStoreWithMiddleware = applyMiddleware(thunk)(createStore);
    // const store = createStoreWithMiddleware(rootReducer);
    const store = createStore(
        rootReducer,
        applyMiddleware(thunk)
    );
    if (module.hot) {
        module.hot.accept(() => {
            const nextRootReducer = require('../reducers/index').default;
            store.replaceReducer(nextRootReducer);
        });
    }
    return store;
}