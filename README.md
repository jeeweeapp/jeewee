# Setup

## (Optional) Install yarn

https://yarnpkg.com/en/docs/install#alternatives-tab

## Init react native project

https://facebook.github.io/react-native/docs/getting-started.html

Be careful to have only 1 android SDK path otherwise they might conflict.

# Install

Follow carefully https://facebook.github.io/react-native/docs/getting-started.html#content
If you have multiple android SDK location be carefull to use the proper one
(e.g. my android studio saves into $HOME/apps/android-sdk-linux)

Also configure Genymotion to use the same SDK

# Run

- Start the emulator (With Mirko being the name of the emulator)

    genymotion
    

NOTE: If could not make Genymotion to work start a normal Android device:

        emulator -avd Mirko

- Start the emulator and run a device with the same SDK required by React (6.0)
- Run

        react-native start
        react-native run-android
        
# Test

    yarn run test
    
It runs the 'test' script under package.json using Yarn.
It looks into the __tests__ folder and executes all the test suites.

# Deploy

## New method

    npm run bundle-android
    npm run assemble-debug
    
Or
    
    npm run assemble-release
    
## Old method

    cd android && ./gradlew assembleRelease
    
The app packages is available at `app/build/outputs/apk/app-{ARCHITECTURE}-release.apk`

In case of errors:

    cd myproject
    react-native start > /dev/null 2>&1 &
    curl "http://localhost:8081/index.android.bundle?platform=android" -o "android/app/src/main/assets/index.android.bundle"

    cd android/ && ./gradlew assembleRelease
    
Or

    cd android/ && ./gradlew assembleDebug
    


# DEV

## Install new react packages

Automatic linking has some issues so some packages needs manual linking.
It might be necessary to add SDK packages. If so run:

    <android-skd-installation-root>/android-sdk-linux/tools/android
    
Note that

- "Supported operating systems are >= Android 4.1 (API 16) and >= iOS 8.0."
- Installing via terminal AND via Android studio might lead to problems (different locations). So just pick one or double check what you are doing
- SDK updates are super huge and slow

## Notes

- enableProguardInReleaseBuilds is DISABLED in build.gradle because it was causing the release app to crash.
Further investigation needed.

- enableSeparateBuildPerCPUArchitecture is enabled in build.gradle

- remember that react-native-device-info needs linking

        react-native link react-native-device-info

- remember to link:

        react-native link react-native-vector-icons

- remember that https://github.com/oblador/react-native-vector-icons needs some gradle config    

- in order to use react-native-config I had to add

    apply from: "../../node_modules/react-native-config/android/dotenv.gradle"
    
in build.gradle (note that is different from what the plugin readme says)


