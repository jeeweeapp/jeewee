#!/bin/bash

EMULATOR_TIME=10
RN_START_TIME=4
RUN_TIME=18

(terminator -e 'emulator -avd Mirko' & echo Start emulator &) &&
(sleep $EMULATOR_TIME && (terminator -e 'react-native start' & echo Start RN... &)) &&
(sleep $RN_START_TIME && (terminator -e 'react-native run-android' & echo Start app... &)) &&
(sleep $RUN_TIME && (terminator -e 'react-native log-android' & echo Start logs...&))