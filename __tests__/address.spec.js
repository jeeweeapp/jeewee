'use strict';

// so we can console.log and see it in the terminal
jest.autoMockOff();

const addressUtils = require('../src/utils/addressUtils');
const mockedData = require('./mock/lux-ville.mock.json');

describe('Search for address: ', () => {
    it('should return empty array because string is not found', () => {
        expect(addressUtils.searchAddress(
            mockedData,
            'missing string'
        ).length).toBe(0);
    });

    it('should find 1 address', () => {
        expect(addressUtils.searchAddress(
            mockedData,
            'Something'
        ).length).toBe(1);
    });

    it('should find 4 addresses', () => {
        expect(addressUtils.searchAddress(
            mockedData,
            'Rue'
        ).length).toBe(4);
    });

    it('should return empty array if passing empty string', () => {
        expect(addressUtils.searchAddress(
            mockedData,
            ''
        ).length).toBe(0);
    });

    it('should return empty array if passing a number', () => {
        expect(addressUtils.searchAddress(
            mockedData,
            666
        ).length).toBe(0);
    });

    it('should return empty array', () => {
        expect(addressUtils.searchAddress(
            [],
            'test string'
        ).length).toBe(0);
    });

    it('should notice that the data are not properly formatted and return empty array', () => {
        expect(addressUtils.searchAddress(
            [
                {
                    "number": "252",
                    "zip": "1220",
                    "city": "Luxembourg"
                }
            ],
            'Some address'
        ).length).toBe(0);
    });

    it('should find 1 result', () => {
        expect(addressUtils.searchAddress(
            [
                {
                    "coord": {
                        "e": 6.12802553435761,
                        "n": 49.6491977937532
                    },
                    "rue": "Rue de Beggen",
                    "number": "252",
                    "zip": "1220",
                    "city": "Luxembourg"
                }
            ],
            'beggen'
        ).length).toBe(1);
    });

    it('should find 1 result when providing the house number', () => {
        let searchResult = addressUtils.searchAddress(
            [
                {
                    "coord": {
                        "e": 6.12802553435761,
                        "n": 49.6491977937532
                    },
                    "rue": "Rue de Beggen",
                    "number": "252",
                    "zip": "1220",
                    "city": "Luxembourg"
                },
                {
                    "coord": {
                        "e": 6.12802553435761,
                        "n": 49.6491977937532
                    },
                    "rue": "Rue de Beggen",
                    "number": "55",
                    "zip": "1220",
                    "city": "Luxembourg"
                }
            ],
            'beggen 55'
        );
        expect(searchResult.length).toBe(1);
    });

    it('should find 1 result if passing house number in weird position', () => {
        let searchResult = addressUtils.searchAddress(
            [
                {
                    "rue": "Rue Frantz Seimetz",
                    "number": "26",
                    "zip": "2531",
                    "city": "Luxembourg"
                },
                {
                    "rue": "Rue de Beggen",
                    "number": "55",
                    "zip": "1220",
                    "city": "Luxembourg"
                }
            ],
            '26 seimetz'
        );
        expect(searchResult.length).toBe(1);
    });

    it('should not need spaces', () => {
        let searchResult = addressUtils.searchAddress(
            [
                {
                    "rue": "Rue Frantz Seimetz",
                    "number": "26",
                    "zip": "2531",
                    "city": "Luxembourg"
                },
                {
                    "rue": "Rue de Beggen",
                    "number": "55",
                    "zip": "1220",
                    "city": "Luxembourg"
                }
            ],
            '26seimetz'
        );
        expect(searchResult.length).toBe(1);
    });

    it('should not need spaces', () => {
        let searchResult = addressUtils.searchAddress(
            [
                {
                    "rue": "Rue Frantz Seimetz",
                    "number": "26",
                    "zip": "2531",
                    "city": "Luxembourg"
                },
                {
                    "rue": "Rue de Beggen",
                    "number": "55",
                    "zip": "1220",
                    "city": "Luxembourg"
                }
            ],
            'beggen55'
        );
        expect(searchResult.length).toBe(1);
    });

    it('should return all addresses if the number is not found', () => {
        let searchResult = addressUtils.searchAddress(
            [
                {
                    "rue": "Rue de Beggen",
                    "number": "26",
                    "zip": "2531",
                    "city": "Luxembourg"
                },
                {
                    "rue": "Rue de Beggen",
                    "number": "55",
                    "zip": "1220",
                    "city": "Luxembourg"
                }
            ],
            '32 Beggen'
        );
        expect(searchResult.length).toBe(2);
    });

    it('should handle house numbers that contains letters', () => {
        let searchResult = addressUtils.searchAddress(
            [
                {
                    "rue": "Rue de la station",
                    "number": "3C",
                    "zip": "2531",
                    "city": "Luxembourg"
                },
                {
                    "rue": "Rue de la station",
                    "number": "4",
                    "zip": "2531",
                    "city": "Luxembourg"
                }
            ],
            'station 3C'
        );
        expect(searchResult.length).toBe(1);
    });
});