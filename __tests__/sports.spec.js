'use strict';

// so we can console.log and see it in the terminal
jest.autoMockOff();

const utils = require('../src/utils/utils');
const mockedData = require('./mock/sports.mock.json');

describe('Search for sport: ', () => {
    it('should return empty array because string is not found', () => {
        expect(utils.filterByPropNameWithRegex(
            mockedData,
            'missing string'
        ).length).toBe(0);
    });

    it('should find 1 address', () => {
        expect(utils.filterByPropNameWithRegex(
            mockedData,
            'football'
        ).length).toBe(1);
    });
});