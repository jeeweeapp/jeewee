'use strict';

// so we can console.log and see it in the terminal
jest.autoMockOff();

const utils = require('../src/utils/utils');

describe('Calculate distance: ', () => {
    it('calculates from Rome to Rio', () => {
        expect(utils.getLinearDistanceBetween(
            {
                n: 41.886731,
                e: 12.493393
            },
            {
                n: -22.906381,
                e: -43.181690
            }
        )).toBe(9197.4);
    });

    it('calculates from Frantz Seimetz to Kirchberg', () => {
        expect(utils.getLinearDistanceBetween(
            {
                n: 49.6236735,
                e: 6.1189015
            },
            {
                n: 49.631897,
                e: 6.171275
            }
        )).toBe(3.88);
    });

    it('calculates from Am Bongert to Parc Merl', () => {
        expect(utils.getLinearDistanceBetween(
            {
                n: 49.6003553,
                e: 6.0918993
            },
            {
                n: 49.606363,
                e: 6.111859
            }
        )).toBe(1.59);
    });
});