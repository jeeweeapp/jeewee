// Android code here!!

// import a library to help create component
import React, {Component} from 'react';

// technique called import destructuring
import {
    AppRegistry,
    Navigator,
    View
} from 'react-native';

import { Provider } from "react-redux";
import configureStore from './src/store/configureStore';
import NavigationRootContainer from './src/containers/NavigationRootContainer';
import Config from 'react-native-config';

import {
    GoogleAnalyticsTracker
} from 'react-native-google-analytics-bridge';

const store = configureStore();

let tracker1 = new GoogleAnalyticsTracker(Config.GOOGLE_ANALYTICS);
tracker1.allowIDFA(true);
tracker1.trackScreenView('OpenApp');
tracker1.trackEvent('OpenAppCategory', 'OpenAppAction');


class JeeWee extends Component {

    render() {
        return (
            <Provider store={store}>
                <NavigationRootContainer />
            </Provider>
        );
    }
}

AppRegistry.registerComponent('Jeewee', () => JeeWee);